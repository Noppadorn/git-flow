function clickChangeColor(typeName) {
	const divMaster = $('.div-master');
	const txtMasterColor = $('.indiv-center');
	if (typeName == 'content-s') {
		divMaster.color({ background: '#ff0' });
		txtMasterColor.color({ color: '#000' });
	} else if (typeName == 'content-m') {
		divMaster.color({ background: '#000' });
		txtMasterColor.color({ color: '#fff' });
	} else if (typeName == 'content-l') {
		divMaster.color({ background: '#f9f' });
		txtMasterColor.color({ color: '#09f' });
	}
}