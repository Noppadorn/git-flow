"use strict";
$(document).ready(function(){

	// this function calling changeColor plugin
	function clickChangeColor(btnType) {
		const divMaster = $('.div-master');
		const txtMasterColor = $('.indiv-center');
		if (btnType == 'content-s') {
			// $( divMaster ).animate({
      //  backgroundColor: "#ff0",
      //  color: "#000"
      //}, 1000 );
			divMaster.color({ background: '#ff0' });
			txtMasterColor.color({ color: '#000' });
		} else if (btnType == 'content-m') {
			// $( divMaster ).animate({
      //  backgroundColor: "#000",
      //  color: "#fff"
      //}, 1000 );
			divMaster.color({ background: '#000' });
			txtMasterColor.color({ color: '#fff' });
		} else if (btnType == 'content-l') {
			// $( divMaster ).animate({
      //  backgroundColor: "#f9f",
      //  color: "#09f"
      //}, 1000 );
			divMaster.color({ background: '#f9f' });
			txtMasterColor.color({ color: '#09f' });
		}
	}

	function displayActive(btnType) {
		const typeName = `.${btnType}`;
		const content = $(typeName);
		if (content.hasClass('active')) {
			content.removeClass('active');
			content.hide();
		} else {
			content.addClass('active');
			content.show();
		}

		const effect = 'slide';
		const duration = 500;

		if (btnType == 'content-s') {
			let options = { direction: 'up' };
			content.toggle(effect, options, duration);
		} else if (btnType == 'content-m') {
			let options = { direction: 'left' };
			content.toggle(effect, options, duration);
		} else if (btnType == 'content-l') {
			let options = { direction: 'right' };
			content.toggle(effect, options, duration);
		}
	}

  $('.btn button').click(function(){
  	const btnType = $(this).attr('data-content');
  	displayActive(btnType);
  	clickChangeColor(btnType);
  });
});